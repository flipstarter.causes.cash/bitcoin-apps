#!/usr/bin/env node

/* Import module.s */
const Bfp = require('../index').bfp
const BfpUtils = require('../lib/utils')
const fs = require('fs')
const superagent = require('superagent')

/* Initialize BFP. */
const bfp = new Bfp()

;(async function () {
    /* Wallet details. */
    const mnemonic = require('../.secrets').mnemonic
    const fundingWif = require('../.secrets').wif

    const info = BfpUtils
        .getFileUploadPaymentInfoFromSeedPhrase(mnemonic)
    console.log('\nWallet info', info)

    /* Set funding address. */
    const fundingAddress = info.cashAddress
    console.log('\nFunding address:', fundingAddress)

    /* Set file receiver address. */
    const fileReceiverAddress = info.cashAddress
    console.log('\nFile receiver address:', fileReceiverAddress)

    /* Set target. */
    const target = `https://insomnia.devops.cash/v1/address/utxos/${fundingAddress}`

    /* Request UTXOs. */
    const utxos = (await superagent
        .get(target)
        .catch(err => console.error(err))).body
    console.log('UTXOS', utxos.utxos)

    /* Initialize UTXO. */
    let fundingUtxo = null

    /* Find largest UTXO. */
    utxos.utxos.forEach(_utxo => {
        /* Validate UTXO. */
        if (fundingUtxo === null || _utxo.value > fundingUtxo.satoshis) {
            /* Set funding UTXO. */
            fundingUtxo = {
                txid: _utxo.tx_hash,
                satoshis: _utxo.value,
                vout: _utxo.tx_pos,
            }
        }
    })
    console.log('\nFunding UTXO', fundingUtxo)

    /* File details. */
    // const filename = 'bitcoin-icon-small'
    // const filename = 'tenk'
    // const filename = 'elevenk'
    const filename = 'index'
    // const fileext = '.bin'
    // const fileext = '.png'
    // const fileext = '.html'
    const fileext = '.bml'
    // const filesize = 8580
    // const filesize = 913
    const filesize = 243

    /* Initialize buffer. */
    const buf = Buffer.alloc(filesize)

    /**
     * Sign Progress Callback
     */
    const signProgressCallback = (_progress) => {
        console.log('signProgressCallback', _progress)
    }

    /**
     * Sign Finished Callback
     */
    const signFinishedCallback = (_progress) => {
        console.log('signFinishedCallback', _progress)
    }

    /**
     * Upload Progress Callback
     */
    const uploadProgressCallback = (_progress) => {
        console.log('uploadProgressCallback', _progress)
    }

    /**
     * Upload Finished Callback
     */
    const uploadFinishedCallback = (_progress) => {
        console.log('uploadFinishedCallback', _progress)
    }

    /* Open file. */
    fs.open(`${filename}${fileext}`, 'r', (status, fd) => {
        /* Validate status. */
        if (status) {
            return console.log('\nFile status:', status)
        }

        /* Read file. */
        fs.read(fd, buf, 0, filesize, 0, async (err, num) => {
            if (err) {
                console.error('ERROR:', num, err)
            }

            /* Upload buffer. */
            // const result = await bfp.uploadFileHashOnly(
            const result = await bfp.uploadFile(
                fundingUtxo,
                fundingAddress,
                fundingWif,
                buf, // fileDataArrayBuffer
                filename,
                fileext,
                null, // prevFileSha256Hex
                null, // fileExternalUri
                fileReceiverAddress,
                signProgressCallback,
                signFinishedCallback,
                uploadProgressCallback,
                uploadFinishedCallback,
                500 // delay_ms
            ).catch(err => console.error(err))

            if (result) {
                console.log('\nUpload complete!')
                console.log(result)
            } else {
                console.log('\nUpload failed!')
            }

        })
    })

    // Wait for download to complete -- Mario.png TAKES ABOUT 6 SEC TO DOWNLOAD!

    // 2 - result includes a boolean check telling you if the file's sha256 matches the file's metadata```
    // if (result.passesHashCheck) {
    //     console.log(`\nSuccess: downloaded file sha256 matches file's metadata`)
    // }

    // 3 - do something with the file...
    // const fileBuffer = result.fileBuf
    // const filename = result.filename
    // const fileext = result.fileext
    // console.log('\nFile result:', result)
    // console.log('\nFile buffer:', fileBuffer)

    // const buf = Buffer.from(fileBuffer, 'base64')
    // fs.writeFile(`${filename}${fileext}`, buf, 'base64', () => {
    //     console.log('file written!')
    // })
})()
