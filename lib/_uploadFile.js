/* Import modules. */
const BfpUtils = require('./utils')
const debug = require('debug')('bitcoinfilesjs-upload-file')
// const util = require('util')

/* Set sleep delay. */
const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))

/* Set chunk size. */
const CHUNK_SIZE = 220 // NOTE: MAX is 223

/**
 * Upload File
 */
const uploadFile = async function (
    _fundingUtxo,
    _fundingAddress,
    _fundingWif,
    _fileDataArrayBuffer,
    _fileName,
    _fileExt,
    _prevFileSha256Hex,
    _fileExternalUri,
    _fileReceiverAddress,
    _signProgressCallback,
    _signFinishedCallback,
    _uploadProgressCallback,
    _uploadFinishedCallback,
    _delay_ms = 500
) {
    const fileSize = _fileDataArrayBuffer.byteLength
    // console.log('\nFILE SIZE', fileSize)

    const hash = this.bitbox.Crypto
        .sha256(Buffer.from(_fileDataArrayBuffer)).toString('hex')

    // chunks
    const chunks = []

    /* Calculate chunk count. */
    let chunkCount = Math.floor(fileSize / CHUNK_SIZE)

    for (let nId = 0; nId < chunkCount; nId++) {
        chunks.push(_fileDataArrayBuffer.slice(nId * CHUNK_SIZE, (nId + 1) * CHUNK_SIZE))
    }

    /* Validate chunk size divisor. */
    if (fileSize % CHUNK_SIZE) {
        chunks[chunkCount] = _fileDataArrayBuffer.slice(chunkCount * CHUNK_SIZE, fileSize)

        /* Increment chunk count. */
        chunkCount++
    }
    debug('\nChunk count', chunkCount)

    /* Build empty meta data OP_RETURN. */
    // NOTE: Estimate cost.
    const configEmptyMetaOpReturn = {
        msgType: 1,
        chunkCount,
        fileName: _fileName,
        fileExt: _fileExt,
        fileSize,
        fileSha256Hex: hash,
        prevFileSha256Hex: _prevFileSha256Hex,
        fileUri: _fileExternalUri
    }

    /* Initialize transactions. */
    const transactions = []

    /* Initialize progress difference. */
    // NOTE: show progress
    let nDiff = 100 / chunkCount

    /* Initialize current position. */
    let nCurPos = 0 // FIXME: Why hasn't the linter picked this up??

    for (let nId = 0; nId < chunkCount; nId++) {
        // build chunk data OpReturn
        const chunkOpReturn = BfpUtils.buildDataChunkOpReturn(chunks[nId])

        let txid = null
        let satoshis = 0
        let vout = 1

        if (nId === 0) {
            txid = _fundingUtxo.txid
            satoshis = _fundingUtxo.satoshis
            vout = _fundingUtxo.vout
        } else {
            txid = transactions[nId - 1].getId()
            if (!transactions[nId - 1].outs[1]) console.log('\nWHERE IS IT', nId, transactions.length, transactions[nId - 1]);
            satoshis = transactions[nId - 1].outs[1].value
        }

        /* Build chunk data transaction. */
        const configChunkTx = {
            bfpChunkOpReturn: chunkOpReturn,
            input_utxo: {
                address: _fundingAddress,
                txid,
                vout,
                satoshis,
                wif: _fundingWif
            }
        }

        /* Build transaction chunks. */
        const chunksTx = require('./_buildChunkTx').bind(this)(configChunkTx)

        if (nId === chunkCount - 1) {
            /* Build "empty" metadata OP_RETURN. */
            const emptyOpReturn = BfpUtils.buildMetadataOpReturn(configEmptyMetaOpReturn)

            const capacity = 223 - emptyOpReturn.length

            if (capacity >= chunks[nId].byteLength) {
                // finish with just a single metadata txn
                // build meta data OpReturn
                let configMetaOpReturn = {
                    msgType: 1,
                    chunkCount,
                    fileName: _fileName,
                    fileExt: _fileExt,
                    fileSize,
                    fileSha256Hex: hash,
                    prevFileSha256Hex: _prevFileSha256Hex,
                    fileUri: _fileExternalUri,
                    chunkData: chunks[nId],
                }

                /* Build metadata OP_RETURN. */
                let metaOpReturn = BfpUtils
                    .buildMetadataOpReturn(configMetaOpReturn)

                // build meta data transaction
                let configMetaTx = {
                    bfpMetadataOpReturn: metaOpReturn,
                    input_utxo: {
                        txid,
                        vout,
                        satoshis,
                        wif: _fundingWif
                    },
                    fileReceiverAddress: (_fileReceiverAddress !== null)
                        ? _fileReceiverAddress : _fundingAddress
                }

                /* Build metadata transaction. */
                const metaTx = require('./_buildMetadataTx').bind(this)(configMetaTx)

                /* Add transaction. */
                // console.log('\nFIRST', nId, metaTx)
                transactions.push(metaTx)
            } else {
                // NOTE: Finish with both chunk txn and then final empty metadata txn.
                transactions.push(chunksTx)

                /* Build metadata OP_RETURN. */
                let metaOpReturn = BfpUtils
                    .buildMetadataOpReturn(configEmptyMetaOpReturn)

                // build meta data transaction
                let configMetaTx = {
                    bfpMetadataOpReturn: metaOpReturn,
                    input_utxo: {
                        txid: chunksTx.getId(),
                        vout,
                        satoshis: chunksTx.outs[1].value,
                        wif: _fundingWif
                    },
                    fileReceiverAddress: (_fileReceiverAddress !== null)
                        ? _fileReceiverAddress : _fundingAddress
                }

                /* Build metadata transaction. */
                const metaTx = require('./_buildMetadataTx').bind(this)(configMetaTx)
                // console.log('\nMETA TX', metaTx)

                /* Add transaction. */
                console.log('\nSECOND', nId, metaTx)
                transactions.push(metaTx)
            }
        } else { // not last transaction
            /* Add transaction. */
            // console.log('\nTHIRD', nId, chunksTx)
            transactions.push(chunksTx)
        }

        // sign progress
        if (_signProgressCallback !== null) {
            _signProgressCallback(nCurPos)
        }

        nCurPos += nDiff
    }

    // progress : signing finished
    if (_signFinishedCallback !== null) {
        _signFinishedCallback()
    }

    //* ** sending transaction
    nDiff = 100 / transactions.length
    nCurPos = 0

    if (_uploadProgressCallback !== null) {
        _uploadProgressCallback(0)
    }

    /* Initialize transaction id. */
    let bfTxid = null

    /* Handle transactions. */
    for (let nId = 0; nId < transactions.length; nId++) {
        console.log('\ntransaction: ', transactions[nId].toHex())
        // console.log('\ndecodeRawTransaction', util.inspect(await this.bitbox.RawTransactions.decodeRawTransaction(transactions[nId].toHex()), null, 4, true))

        /* Send transaction. */
        bfTxid = await this.network.sendTxWithRetry(transactions[nId].toHex())
            .catch(err => console.error(err))

        // progress
        if (_uploadProgressCallback !== null) {
            _uploadProgressCallback(nCurPos)
        }

        nCurPos += nDiff

        /* Sleep. */
        // NOTE: Delay between transactions.
        await sleep(_delay_ms)
    }

    /* Set prefix. */
    if (bfTxid) {
        bfTxid = 'bitcoinfile:' + bfTxid
    }

    if (_uploadFinishedCallback !== null) {
        _uploadFinishedCallback(bfTxid)
    }

    /* Return transaction id. */
    return bfTxid
}

/* Export module. */
module.exports = uploadFile
