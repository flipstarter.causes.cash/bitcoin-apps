// import { BITBOX, TransactionBuilder } from 'bitbox-sdk';
// const BfpDb = require('./bitdb')
const BfpNetwork = require('./network')
const BfpUtils = require('./utils')
const BITBOX = require('bitbox-sdk').BITBOX
const debug = require('debug')('bitcoinfilesjs')

/**
BITCOIN FILES PROTOCOL INTERFACES
---------------------------------

interface utxo
    txid: string;
    vout: number;
    amount?: number;
    satoshis: number;
    height?: number;
    confirmations?: number;
    wif?: string;
    address?: string;

interface FileMetadata
    msgType: number;
    chunkCount: number;
    fileName?: string;
    fileExt?: string;
    fileSize: number;
    fileSha256Hex?: string;
    prevFileSha256Hex?: string;
    fileUri?: string;
    chunkData?: Buffer;

interface FundingTxnConfig
    outputAddress: string;
    fundingAmountSatoshis: number;
    input_utxos: utxo[];

interface MetadataTxnConfig
    bfpMetadataOpReturn: Buffer;
    input_utxo: utxo
    fileReceiverAddress: string;

interface DataChunkTxnConfig
    bfpChunkOpReturn: Buffer;
    input_utxo: utxo

*/

/**
 * Bitcoin Files Protocol
 */
class BFP {
    /* Constructor. */
    constructor(_network = 'mainnet') {
        debug('Constructor (network):', _network)

        /* Initialize BITBOX. */
        this.bitbox = new BITBOX()

        /* Initialize network. */
        // this.network = new BFPNetwork(this.BITBOX, grpcUrl)
        this.network = new BfpNetwork(this.bitbox)
        // console.log('NETWORK', this.network)

        /* Initialize BitDB. */
        // this.bitdb = new BfpDb(_network)

        /* Initialize Utilities. */
        this.utils = new BfpUtils(this.bitbox)
    }

    /**
     * Upload Hash ONLY (object)
     */
    async uploadHashOnlyObject(
        _type,
        _fundingUtxo,
        _fundingAddress,
        _fundingWif,
        _objectDataArrayBuffer,
        _objectName,
        _objectExt,
        _prevObjectSha256Hex,
        _objectExternalUri,
        _objectReceiverAddress,
        _signProgressCallback,
        _signFinishedCallback,
        _uploadProgressCallback,
        _uploadFinishedCallback
    ) {
        return require('./_uploadHashOnlyObject').bind(this)(
            _type,
            _fundingUtxo,
            _fundingAddress,
            _fundingWif,
            _objectDataArrayBuffer,
            _objectName,
            _objectExt,
            _prevObjectSha256Hex,
            _objectExternalUri,
            _objectReceiverAddress,
            _signProgressCallback,
            _signFinishedCallback,
            _uploadProgressCallback,
            _uploadFinishedCallback
        )
    }

    /**
     * Upload Folder Hash ONLY
     */
    async uploadFolderHashOnly(
        _fundingUtxo,
        _fundingAddress,
        _fundingWif,
        _folderDataArrayBuffer,
        _folderName,
        _folderExt,
        _prevFolderSha256Hex,
        _folderExternalUri,
        _folderReceiverAddress,
        _signProgressCallback,
        _signFinishedCallback,
        _uploadProgressCallback,
        _uploadFinishedCallback
    ) {
        return await this.uploadHashOnlyObject(
            3, // type 3 = folder
            _fundingUtxo,
            _fundingAddress,
            _fundingWif,
            _folderDataArrayBuffer,
            _folderName,
            _folderExt,
            _prevFolderSha256Hex,
            _folderExternalUri,
            _folderReceiverAddress,
            _signProgressCallback,
            _signFinishedCallback,
            _uploadProgressCallback,
            _uploadFinishedCallback
        )
    }

    /**
     * Upload File Hash ONLY
     */
    async uploadFileHashOnly(
        _fundingUtxo,
        _fundingAddress,
        _fundingWif,
        _fileDataArrayBuffer,
        _fileName,
        _fileExt,
        _prevFileSha256Hex,
        _fileExternalUri,
        _fileReceiverAddress,
        _signProgressCallback,
        _signFinishedCallback,
        _uploadProgressCallback,
        _uploadFinishedCallback
    ) {
        return await this.uploadHashOnlyObject(
            1, // type 1 = file
            _fundingUtxo,
            _fundingAddress,
            _fundingWif,
            _fileDataArrayBuffer,
            _fileName,
            _fileExt,
            _prevFileSha256Hex,
            _fileExternalUri,
            _fileReceiverAddress,
            _signProgressCallback,
            _signFinishedCallback,
            _uploadProgressCallback,
            _uploadFinishedCallback
        )
    }

    /**
     * Upload File
     */
    async uploadFile(
        _fundingUtxo,
        _fundingAddress,
        _fundingWif,
        _fileDataArrayBuffer,
        _fileName,
        _fileExt,
        _prevFileSha256Hex,
        _fileExternalUri,
        _fileReceiverAddress,
        _signProgressCallback,
        _signFinishedCallback,
        _uploadProgressCallback,
        _uploadFinishedCallback,
        _delay_ms = 500
    ) {
        return require('./_uploadFile').bind(this)(
            _fundingUtxo,
            _fundingAddress,
            _fundingWif,
            _fileDataArrayBuffer,
            _fileName,
            _fileExt,
            _prevFileSha256Hex,
            _fileExternalUri,
            _fileReceiverAddress,
            _signProgressCallback,
            _signFinishedCallback,
            _uploadProgressCallback,
            _uploadFinishedCallback,
            _delay_ms
        )
    }

    /**
     * Download File
     */
    async downloadFile(_bfpUri, _progressCallback) {
        return require('./_downloadFile').bind(this)(_bfpUri, _progressCallback)
    }
}

/* Export module. */
module.exports = BFP
