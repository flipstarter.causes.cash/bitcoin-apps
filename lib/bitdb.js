// import Axios, * as axios from 'axios';
const debug = require('debug')('bitcoinfilesjs-bitdb')
const superagent = require('superagent')

/**
 * Bitcoin Files Protocol (BFP) BitDB
 */
class BFPBitDB {
    /* Constructor. */
    constructor(_network) {
        debug('Constructor (network):', _network)

        // this.bitDbUrl = _network === 'mainnet' ? 'https://bitdb.bitcoin.com/q/' : 'https://tbitdb.bitcoin.com/q/'
        this.bitDbUrl = _network === 'mainnet' ? 'https://bitdb.fountainhead.cash/q/' : 'https://tbitdb.fountainhead.cash/q/'
    }

    /**
     * Get File Metadata
     */
    async getFileMetadata(_txid, _apiKey = null) {
        _txid = _txid.replace('bitcoinfile:', '')
        _txid = _txid.replace('bitcoinfiles:', '')

        /* Build query. */
        const query = {
            v: 3, // version
            q: {
                find: {
                    'tx.h': _txid,
                    'out.h1': '424D4C00', // BML - Bitcoin Markup Language
                    'out.h2': '01' // ??
                }
            },
            r: {
                f: `[ .[] | { timestamp: (if .blk? then (.blk.t | strftime("%Y-%m-%d %H:%M")) else null end), chunks: .out[0].h3, filename: .out[0].s4, fileext: .out[0].s5, size: .out[0].h6, sha256: .out[0].h7, prev_sha256: .out[0].h8, ext_uri: .out[0].s9, URI: "bitcoinfile:\\(.tx.h)" } ]`
            }
        }

        /*
        Example response format:
        {
            filename: 'tes158',
            fileext: '.json',
            size: '017a',
            sha256: '018321383bf2672befe28629d1e159af812260268a8aa77bbd4ec27489d65b58',
            prev_sha256: '',
            ext_uri: ''
        }
        */

        /* Convert to JSON string. */
        const json_str = JSON.stringify(query)

        /* Convert to Base64. */
        const data = Buffer.from(json_str).toString('base64')

        /* Set target. */
        const target = this.bitDbUrl + data

        /* Make request. */
        const response = await superagent
            .get(target)
            .set('key', _apiKey)
            .catch(err => console.error(err))
        console.log('RESPONSE', response)

        // FIXME: Handle JSON body.

        /* Validate response. */
        if (response.status === 'error') {
            throw new Error(response.message || 'API error message missing')
        }

        /* Validate response. */
        if (!response.body) {
            throw new Error(response || 'API error message missing')
        }

        /* Set body. */
        const body = response.body

        /* Initialize list. */
        const list = []

        /* Validate confirmed. */
        if (body.c) {
            /* Add confirmed. */
            list.push(...body.c)
        }

        /* Validate unconfirmed. */
        if (body.u) {
            /* Add unconfirmed. */
            list.push(...body.u)
        }

        /* Validate list. */
        if (list.length === 0) {
            throw new Error('File not found')
        }

        console.log('BITDB response (list):', list)
        console.log('BITDB response (list item #0):', list[0])

        /* Return list item #0 */
        // FIXME: Why are we only returning item #0??
        return list[0]
    }
}

/* Export module. */
module.exports = BFPBitDB
